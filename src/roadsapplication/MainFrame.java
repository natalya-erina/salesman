package roadsapplication;

import javax.swing.table.DefaultTableModel;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Наталья
 */
public class MainFrame extends javax.swing.JFrame {

    private Cities cities;
    /**
     * Creates new form MainFrame
     */
    public MainFrame() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabelNumberCities = new javax.swing.JLabel();
        jTextFieldNumberCities = new javax.swing.JTextField();
        jButtonGenerateMatrix = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableDistances = new javax.swing.JTable();
        jButtonSolve = new javax.swing.JButton();
        jTextFieldMinWay = new javax.swing.JTextField();
        jLabelTimeMin = new javax.swing.JLabel();
        jLabelTimeRand = new javax.swing.JLabel();
        jTextFieldTimeMin = new javax.swing.JTextField();
        jTextFieldTimeRand = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabelNumberCities.setText("Кол-во городов:");
        jLabelNumberCities.setName(""); // NOI18N

        jButtonGenerateMatrix.setText("Сгенерировать матрицу");
        jButtonGenerateMatrix.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonGenerateMatrixActionPerformed(evt);
            }
        });

        jTableDistances.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(jTableDistances);

        jButtonSolve.setText("Минимальный путь");
        jButtonSolve.setEnabled(false);
        jButtonSolve.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSolveActionPerformed(evt);
            }
        });

        jTextFieldMinWay.setEditable(false);

        jLabelTimeMin.setText("Время работы алгоритма с наименьшими изменениями (мс)");

        jLabelTimeRand.setText("Время работы при генерации рандомоных перестановок (мс)");

        jTextFieldTimeMin.setEditable(false);

        jTextFieldTimeRand.setEditable(false);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 461, Short.MAX_VALUE)
                    .addComponent(jLabelTimeRand)
                    .addComponent(jLabelTimeMin)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabelNumberCities)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldNumberCities, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButtonGenerateMatrix, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jTextFieldTimeRand, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldTimeMin, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonSolve, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldMinWay))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelNumberCities)
                    .addComponent(jTextFieldNumberCities, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonGenerateMatrix))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButtonSolve)
                .addGap(18, 18, 18)
                .addComponent(jTextFieldMinWay, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabelTimeMin)
                .addGap(4, 4, 4)
                .addComponent(jTextFieldTimeMin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jLabelTimeRand)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTextFieldTimeRand, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonGenerateMatrixActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonGenerateMatrixActionPerformed
        jTextFieldMinWay.setText("");
        jTextFieldTimeMin.setText("");
        jTextFieldTimeRand.setText("");
        
        int n = Integer.parseInt(jTextFieldNumberCities.getText());
        cities = new Cities(n);
        
        Integer[] columnNames = new Integer[n + 1];
        columnNames[0] = 0;
        for (int i = 1; i < n+1; i++)
            columnNames[i] = i;
        int[][] distances = cities.getDistances();
        Integer[][] newDistances = new Integer[n][n + 1];
        for (int j = 0; j < n; j++) {
            for (int i = 1; i < n + 1; i++)
                newDistances[j][i] = distances[j][i - 1];
            for (int i = 0; i < n; i++)
                newDistances[i][0] = i+1;
        }
        DefaultTableModel dtm = new DefaultTableModel(newDistances, columnNames);
        dtm.setRowCount(n);
        jTableDistances.setModel(dtm);
        jButtonSolve.setEnabled(true);
    }//GEN-LAST:event_jButtonGenerateMatrixActionPerformed

    private void jButtonSolveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSolveActionPerformed
        int[] minWay = cities.getMinWay();
        String textMinWay = "";
        for (int i = 0; i < minWay.length; i++) {
            textMinWay += minWay[i] + " -> ";
        }
        textMinWay += minWay[0];
        cities.getMinWayRandomly();
        jTextFieldMinWay.setText(textMinWay);
        jTextFieldTimeMin.setText(String.valueOf(cities.getTimeMin()));
        jTextFieldTimeRand.setText(String.valueOf(cities.getTimeRand()));
    }//GEN-LAST:event_jButtonSolveActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(MainFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonGenerateMatrix;
    private javax.swing.JButton jButtonSolve;
    private javax.swing.JLabel jLabelNumberCities;
    private javax.swing.JLabel jLabelTimeMin;
    private javax.swing.JLabel jLabelTimeRand;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableDistances;
    private javax.swing.JTextField jTextFieldMinWay;
    private javax.swing.JTextField jTextFieldNumberCities;
    private javax.swing.JTextField jTextFieldTimeMin;
    private javax.swing.JTextField jTextFieldTimeRand;
    // End of variables declaration//GEN-END:variables
}

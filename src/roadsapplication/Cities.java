/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package roadsapplication;

import java.util.Arrays;
import java.util.Random;

/**
 *
 * @author Наталья
 */

/*
Ерина Наталья, 91 группа

4. Коммивояжер. Торговец должен выйти из первого города, посетить по одному разу
в неизвестном порядке города 2,3,4, …. N и вернуться в первый город, чтобы 
путь коммивояжера был кратчайшим. Известна матрица расстояний между городами. 
Необходимо найти кратчайший путь.
b) Перестановки генерируются наименьшими изменениями и в случайном порядке. 
Необходимо сравнить скорость работы алгоритмов.
*/

public class Cities {
    //кол-во городов
    private final int n;
    //матрица расстояний
    private final int[][] distances;
    
    private int[] y;
    private int[] directions;
    
    private int[] minRoad;
    //время работы алгоритма с наименьшими изменениями
    private long timeMin;
    //время работы рандомного алгоритма
    private long timeRand;
    
    public Cities(int aN) {
        n = aN;
        y = new int[n];
        directions = new int[n];
        distances = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i == j) 
                    distances[i][j] = -1;
                else
                    distances[i][j] = randInt(1, 15);
            }
        }
    }
        
    private int randInt(int min, int max) {
        Random rand = new Random();
        int randomNum = rand.nextInt((max - min) + 1) + min;
        return randomNum;
    }
    
    //слить два массива в один
    private int[] concat(int[] a, int[] b) {
        int aLen = a.length;
        int bLen = b.length;
        int[] c= new int[aLen+bLen];
        System.arraycopy(a, 0, c, 0, aLen);
        System.arraycopy(b, 0, c, aLen, bLen);
        return c;
    }
        
    public long getTimeMin() {
        return timeMin;
    }
    
    public long getTimeRand() {
        return timeRand;
    }

    public int[][] getDistances() {
        return distances;
    }
    
    //посчитать суммарное расстояние для перестановки perm
    private int countDistance(int[] perm) {
        int distance = 0;
        for (int i = 0; i < perm.length - 1; i++) {
            distance += distances[perm[i]-1][perm[i+1]-1];
        }
        distance += distances[perm[perm.length - 1] - 1][perm[0] - 1];
        return distance;
    }
    
    //получить следующее число в перестановке, которое нужно менять
    private int getNextChecker(int i) {
        while (i > 0 && ((directions[i] == 1 && y[i] == i-1) || 
                         (directions[i] == -1 && y[i] == 0)))
            --i;
        return i;
    }
        
    //получить следующую перестановку для p 
    private int[] nextPerm(int[] p) {
        int size = p.length - 1;
        int i = getNextChecker(size);
        if (i == 0) return null;
        y[i] += directions[i];
        for (int j = i+1; j <= size; j++)
            directions[j] = -directions[j];
        int k = 0;
        for (int j = 0; j < p.length; j++) {
            int searchedItem = p.length - i + 1;
            if (p[j] == searchedItem) {
                k = j;
                break;
            }
        }
        int tmp = p[k];
        p[k] = p[k + directions[i]];
        p[k + directions[i]] = tmp;
        return p;
    }
    
    //найти минимальную дорогу
    private int[] getMinRoad() {
        for (int i = 0; i < n; i++) {
            y[i] = 0;
            directions[i] = 1;
        }
        //первая перестановка - 1,2,3,...,n
        int[] perm = new int[n];
        for (int i = 0; i < perm.length; i++) {
            perm[i] = i + 1;
        }
        int minDistance = countDistance(perm);
        
        //минимальная дорога - изначально равна perm
        int[] minWay = new int[n];
        System.arraycopy(perm, 0, minWay, 0, perm.length);
        
        int[] nextPerm = nextPerm(perm);
        while (nextPerm != null) {
            int currentDistance = countDistance(nextPerm);
            if (currentDistance < minDistance) {
                minDistance = currentDistance;
                System.arraycopy(nextPerm, 0, minWay, 0, nextPerm.length);
            }
            nextPerm = nextPerm(nextPerm);
        }
        return minWay;
    }
    
    public int[] getMinWay() {
        timeMin = System.currentTimeMillis();
        minRoad = getMinRoad();
        timeMin = System.currentTimeMillis()- timeMin;
        return minRoad;
    }
    
    private int[] getMinRoadRandomly(int[] minRoad) {
        int[] perm = new int[n];
        for (int i = 0; i < perm.length; i++)
            perm[i] = i + 1;
        
        //рандомно ищем перестановку, равную minRoad 
        while (!Arrays.equals(perm, minRoad)){
            int left = randInt(0, n-1);
            int right = randInt(0, n-1);
            while (left == right) {
                right = randInt(0, n-1);
            }

            int tmp = perm[left];
            perm[left] = perm[right];
            perm[right] = tmp;
        }
        return perm;
    }
    
    public int[] getMinWayRandomly() {
        timeRand = System.currentTimeMillis();
        int[] minWay = getMinRoadRandomly(minRoad);
        timeRand = System.currentTimeMillis() - timeRand;
        return minWay;
    }
}
